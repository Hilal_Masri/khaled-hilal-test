import "../components/AdminCard/AdminCard.css";
import API from "../api";
import React, { useEffect, useState } from "react";

export default function (props) {
  const [students, setStudents] = useState([]);
  const [status, setStatus] = useState("");
  const [attendance, setAttendance] = useState([]);



  const getStudentbyDate = async() =>{
    await API.get(`studentattendance`).then((res) => {
      const result = res.data;
      setAttendance(result);
    });
  };

  // const students = []; // all the students
  // const attendance = []; // all the students that has att

  // // inside render
  // students.filter(student => {
  //   return !attendance.find(item => {
  //     return item.StudentId === student.StudentId
  //   })
  // }).map(student => {
  //   return (
  //     <div></div>
  //   )
  // })
  // // inside render

  const getData = async () => {
    await API.get(`studentbysection/${props.idSection}`).then((res) => {
      const result = res.data;
      setStudents(result);
    });
  };

  useEffect(() => {
    getData();
    getStudentbyDate();
  }, [props.idSection]);

  const handleClick = async (a, b) => {
    let reqBody = {
      StudentId: a,
      SectionId: b,
      status: status,
    };
    setStatus("");

    try {
      const l = await API.post(`addsattendance`, reqBody);
    } catch (error) {
      console.log("BIG Error : ", error);
    }
  };
  return (
    <>
    {students.filter(student => {
    return !attendance.find(item => {
      return item.StudentId === student.id
    })
  }).map(student => {
    return (
      <tr>
      <td>
       
      </td>
      <td>{student.FName}</td>
      <td>{student.LName}</td>
      <td>
        <select id="outside" onChange={(e) => setStatus(e.target.value)}>
          <option value={null}>Attendance</option>
          <option value="Present">Present</option>
          <option value="Absent">Absent</option>
          <option value="Late">Late</option>
        </select>
      </td>
      <td>
      <button type="button" class="btn btn-outline-danger" onClick={() =>{handleClick(student.id, student.SectionId)}}>
        Done
      </button>
      </td>
    </tr>    )
  })}
      {/* {students.map((student) => (
        <tr>
          <td>
            <span class="custom-checkbox">
              <input
                type="checkbox"
                id="checkbox1"
                name="options[]"
                value="1"
              />
              <label for="checkbox1"></label>
            </span>
          </td>
          <td>{student.FName}</td>
          <td>{student.LName}</td>
          <td>
            <select id="inside" onChange={(e) => setStatus(e.target.value)}>
              <option value={null}>Attendance</option>
              <option value="Present">Present</option>
              <option value="Absent">Absent</option>
              <option value="Late">Late</option>
            </select>
          </td>
          <button onClick={() =>{handleClick(student.id, student.SectionId)}}>
            Done
          </button>
        </tr>
      ))} */}
    </>
  );
}
