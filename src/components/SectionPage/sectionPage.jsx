import { Link } from "react-router-dom";
import "../../pages/AdminPage/AdminPage.css";
import SectionCard from "../../components/SectionCard/sectionCard";
import React, { useState, useEffect } from "react";
import SideNav from '../../components/SideNav/SideNav'
import API from "../../api";


export default function SectionPage() {
  const [section, setSection] = useState([]);
  const [error, setError] = useState("");

  

  useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:8000/api/ok");
      res
        .json()
        .then((res) => {
          setSection(res);

          console.log(section);
        })
        .catch((err) => console.log(err));
    }


    
    fetchData();
  }, []);

  const deleteSection = async (id) => {
    let r=window.confirm('are you sure ?')
    if(r){
    try {
      let res = await API.delete(`deletesection/${id}`).then((res) => {
         const result = res.data.message;
         alert(result)
         window.location.reload();
     });
     } catch (error) {
       console.log("BIG Error : ", error);
     }
    }
    
  };
  return (
    <>
    <SideNav/>
      <div class="container-xl adminpage">
        <div class="table-responsive">
          <div class="table-wrapper">
            <div class="table-title">
              <div class="row">
                <div class="col-sm-6">
                  <h2>
                    Manage <b>Section</b>
                  </h2>
                </div>
                <div class="col-sm-6">
                  <Link
                    to="/addsection"
                    class="btn"
                    data-toggle="modal"
                  >
                    <i class='bx bxs-plus-circle bx-burst' ></i>
                    <span>Add New Section</span>
                  </Link>
                 
                </div>
              </div>
            </div>
            <table class="table table-striped table-hover">
              <thead>
                <tr>
                  <th>
                    
                  </th>
                  <th>Section</th>
                  <th>Class</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {section.map((se) => (
                  <SectionCard
                    key={se.id}
                    id={se.id}
                    class_Description={se.class_Description}
                    description={se.Description}
                    delete={deleteSection}
                  />
                ))}


              </tbody>
            </table>


            <div class="clearfix">
              <div class="hint-text">
                Showing <b>5</b> out of <b>25</b> entries
              </div>
              <ul class="pagination">
                <li class="page-item disabled">
                  <a href="#">Previous</a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    1
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    2
                  </a>
                </li>
                <li class="page-item active">
                  <a href="#" class="page-link">
                    3
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    4
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    5
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    Next
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

