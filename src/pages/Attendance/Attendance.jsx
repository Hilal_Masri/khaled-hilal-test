import "./Attendance.css";
import Classes_List from "../../components/Classes_List";
import Sections_Class from "../../components/Sections_Class";
import React, { useState } from "react";
import StudentSection from "../../components/StudentSection";
import SideNav from "../../components/SideNav/SideNav";

export default function Attendance(props) {
  const [classs, setclass] = useState("");
  const [section, setsection] = useState("");
  const [students, setstudents] = useState([]);
  let today = new Date();
  var days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  var d = new Date();
  var dayName = days[d.getDay()];

  return (
    <>
      <SideNav />
      <div class="container-xl adminpage">
        <div class="table-responsive">
          <div class="table-wrapper">
            <div class="table-title">
              <div class="row">
                <div class="col-sm-6">
                  <h2>
                    <b> Take Daily Attendance</b>
                  </h2>
                </div>
                <div className="col-sm-6">
                  <b>
                    {dayName +
                      "-" +
                      today.getFullYear() +
                      "-" +
                      (today.getMonth() + 1) +
                      "-" +
                      today.getDate()}
                  </b>
                </div>
              </div>
            </div>
            <div>
              <Classes_List
                onChange={(e) => {
                  setclass(e.target.value);
                }}
              />

              <Sections_Class
                idClass={classs}
                onChange={(e) => {
                  setsection(e.target.value);
                }}
              />
            </div>
            <table class="table table-striped table-hover">
              <thead>
                <tr>
                  <th></th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Attendance</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <StudentSection idSection={section} />
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}