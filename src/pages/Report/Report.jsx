import React, { useEffect, useState } from "react";
import API from "../../api";
import { PieChart, Bar, Pie, Sector, Cell, ResponsiveContainer, Tooltip, BarChart, XAxis, YAxis, CartesianGrid, Legend } from 'recharts';
import './report.css'
import Classes_List from "../../components/Classes_List";
import Sections_Class from "../../components/Sections_Class";
import SideNav from '../../components/SideNav/SideNav'



export default function Report(props) {
  const [classs, setclass] = useState("");
  const [section, setsection] = useState("");
  const [date, setDate] = useState("");



  const [data, Setdata] = useState([]);

  const getData = async (e) => {
    let reqBody = {
      SectionId: section,
      date:date
    };
    try {
      let res = await API.post(`countbystatus`, reqBody).then((res) => {
        const result = res.data;
        Setdata(result);

      });
    } catch (error) {
      console.log("BIG Error : ", error);
    }
  }

  useEffect(() => {
    getData();
  }, [date]);
  return (

    <>

<SideNav/>
      <div class="container-xl adminpage">
        <div class="table-responsive">
          <div class="table-wrapper">
            <div class="table-title">
              <div class="row">
                <div class="col-sm-6">
                  <h2>
                    <b>Attendance students</b>
                  </h2>
                </div>
                <div class="col-sm-6"></div>
              </div>
            </div>
            <div>
              <Classes_List
                onChange={(e) => {
                  setclass(e.target.value);
                }}
              />

              <Sections_Class
                idClass={classs}
                onChange={(e) => {
                  setsection(e.target.value);
                }}
              />

              <label for="date" id="labeldate" >
                Choose A Date:
              </label>
              <input type="date" name="date" class="dateview" onChange={(e) => {
                setDate(e.target.value);
              }} >

              </input>


            </div>
            <div class="table table-striped table-hover">
              <h3>diag circu:</h3>
              <PieChart width={400} height={400}>
                <Pie
                  data={data}
                  dataKey="value"
                  cx={200}
                  cy={200}
                  outerRadius={130}

                  fill="#8884d8"
                  label
                />
                <Tooltip />

              </PieChart>

              <h3>diag circu:</h3>

              <BarChart
                width={500}
                height={300}
                data={data}
                margin={{
                  top: 5,
                  right: 30,
                  left: 20,
                  bottom: 5,
                }}
                barSize={20}
              >
                <XAxis dataKey="name" scale="point" padding={{ left: 10, right: 10 }} />
                <YAxis />
                <Tooltip />
                <Legend />
                <CartesianGrid strokeDasharray="3 3" />
                <Bar dataKey="value" fill="#8884d8" background={{ fill: '#eee' }} />
              </BarChart>

            </div>
          </div>
        </div>
      </div>
    </>
   







  );



}