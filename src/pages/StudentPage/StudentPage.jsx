import { Link } from "react-router-dom";
import AdminPage from "../AdminPage/AdminPage.css";
import React, { useEffect, useState } from 'react'
import StudentCard from '../../components/StudentCard/StudentCard'
import API from '../../api';
import SideNav from '../../components/SideNav/SideNav'


export default function StudentPage() {

  const [students, setstudents] = useState([]);

  const fetchData = async () => {

    await API.get(`set-sec-class`)
      .then(res => {
        const result = res.data.data;
        setstudents(result);

        
      })
  }



  useEffect(() => {
    fetchData();
  }, []);



  const deletestudent = async (id) => {
    try {
      await API.delete(`deletestudent/${id}`);
      window.location.reload();
    } catch (e) {
      console.log(e)
    };
  };

  return (
<>
<SideNav/>
    <div>

      <div class="container-xl adminpage">
        <div class="table-responsive">
          <div class="table-wrapper">
            <div class="table-title">
              <div class="row">
                <div class="col-sm-6">
                  <h2>
                    Manage <b>Students</b>
                  </h2>
                </div>
                <div class="col-sm-6">
                  <Link
                    to="/addstudent"
                    class="btn "
                    data-toggle="modal"
                  >
                    <i class='bx bxs-plus-circle bx-burst' ></i>
                    <span>Add New Student</span>
                  </Link>
                 
                </div>
              </div>
            </div>
            <table class="table table-striped table-hover">
              <thead>
                <tr>
                  <th>
                    
                  </th>
                  <th>Image</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Class</th>
                  <th>Section</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

                {students.map(student => (
                  <StudentCard
                    key={student.id}
                    id={student.id}
                    fname={student.FName}
                    lname={student.LName}
                    number={student.Number}
                    bloodtype={student.BloodType}
                    email={student.Email}
                    birth={student.BirthDate}
                    adress={student.Adress}
                    image={student.Image}
                    image={student.Image}
                    class_name={student.class_name}
                    section_name={student.section_name}
                    delete={deletestudent}
                  />
                ))}

              </tbody>
            </table>
            <div class="clearfix">
              <div class="hint-text">
                Showing <b>5</b> out of <b>25</b> entries
              </div>
              <ul class="pagination">
                <li class="page-item disabled">
                  <a href="#">Previous</a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    1
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    2
                  </a>
                </li>
                <li class="page-item active">
                  <a href="#" class="page-link">
                    3
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    4
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    5
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    Next
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>


    </div>
    </>
  )
}