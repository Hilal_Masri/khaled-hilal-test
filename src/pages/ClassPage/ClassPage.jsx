import { Link } from "react-router-dom";
import '../AdminPage/AdminPage.css'
import ClassCard from '..//../components/ClassCard/ClassCard'

import React, { useState, useEffect } from "react";
import SideNav from '../../components/SideNav/SideNav'

export default function ClassPage() {
  //const [error, setError] = useState({});

  const [classs, setClass] = useState([]);
  const [error, setError] = useState("");

  useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:8000/api/getclass");
      res
        .json()
        .then((res) => {
          //console.log(res[0]);
          //console.log(res);

          setClass(res);

          console.log(classs);
        })
        .catch((err) => console.log(err));
    }
    fetchData();

  }, []);


  const deleteclass = async (id) => {

    try {
      const response = await fetch(`//localhost:8000/api/deleteclass/${id}`, {
        method: "delete",
      });

      console.log("hello", response);

    } catch (e) {
      setError(e)
    };

    let stateClass = [...classs].filter(cl => cl.id !== id);
    setClass(stateClass);
    alert("deleted successfuly")


  };
  return (
    <>
    <SideNav/>
      <div class="container-xl adminpage">
        <div class="table-responsive">
          <div class="table-wrapper">
            <div class="table-title">
              <div class="row">
                <div class="col-sm-6">
                  <h2>
                    Manage <b>classes</b>
                  </h2>
                </div>
                <div class="col-sm-6">
                  <Link
                    to="/addclass"
                    class="btn "
                    data-toggle="modal"
                  >
                    <i class='bx bxs-plus-circle bx-burst' ></i>
                    <span>Add New class</span>
                  </Link>
                  
                </div>
              </div>
            </div>
            <table class="table table-striped table-hover">
              <thead>
                <tr>
                  <th>
                    
                  </th>
                  <th>Classe</th>
                  <th>Sections</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {classs.map((cl) => (
                  <ClassCard
                    key={cl.id}
                    id={cl.id}
                  description={cl.Description}
                  class_Description={cl.class_Description}
                    delete={deleteclass}
                  />
                ))}
              </tbody>

            </table>
            <div class="clearfix">
              <div class="hint-text">
                Showing <b>5</b> out of <b>25</b> entries
              </div>
              <ul class="pagination">
                <li class="page-item disabled">
                  <a href="#">Previous</a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    1
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    2
                  </a>
                </li>
                <li class="page-item active">
                  <a href="#" class="page-link">
                    3
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    4
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    5
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    Next
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
